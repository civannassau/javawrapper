/*
 * Copyright (c) 2019 Chenoa van Nassau
 * All rights reserved
 */
// load package
package nl.bioinf.civannassau.wekarunner;

// imports
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import java.io.IOException;
import java.util.Arrays;

// make public class
public class MainClassifierRunner {
    // throw exception
    public static void main(String[] args) throws IOException {
        CommandlineParser parser = new CommandlineParser(args);
        // use the formed shortcuts to call different types
        for (String arg : args) {
            // call file
            if (arg.equals("-f")) {
                Wekarunner wekarunner = new Wekarunner();
                String fileName = parser.fetchFile();
                wekarunner.start(fileName);
            }
            // call instance
            if (arg.equals("-i")) {
                InstanceParser instance = new InstanceParser();
                String singInstance = parser.getInstance();
                instance.Instance(singInstance);
                Wekarunner wekarunner = new Wekarunner();
                String fileName = "../../../../../../testdata/newinstance.arff";
                wekarunner.start(fileName);
            }


        // use to call the help on the commandline
        try {
            CommandlineParser command = new CommandlineParser(args);
            if (command.helpRequested()) {
                command.printHelp();
                return;
            }
        } catch (IllegalStateException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            CommandlineParser command = new CommandlineParser(new String[]{});
            command.printHelp();
        }
        }
    }
}