/*
 * Copyright (c) 2019 Chenoa van Nassau
 * All rights reserved
 */
// load package
package nl.bioinf.civannassau.wekarunner;
// imports
import weka.classifiers.functions.SMO;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

//import java.io.File;
//import java.io.InputStream;
//import java.io.ObjectInputStream;
import java.io.IOException;

// make public class
public class Wekarunner {
    // file name of model
    private final String modelFile = "../../testdata/smo.model";
    // call runner plus filename with exception
    public static void main(String[] args, String fileName) throws Exception {
        Wekarunner runner = new Wekarunner();
        runner.start(fileName);
    }
    // load arff file (data)
    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }
    // print instances
    private void printInstances(Instances instances) {
        // get the number of attributes
        int numOfAttributes = instances.numAttributes();

        // print the attributes
        for (int i = 0; i < numOfAttributes; i++) {
            System.out.println("Attribute " + i + " = " + instances.attribute(i));
        }

        // print the class attribute index
        System.out.println("Class index = " + instances.classIndex());

        // get the number of instances
        int numOfInstances = instances.numInstances();

        // print the instances
        for (int i = 0; i < numOfInstances; i++) {
            Instance instance = instances.instance(i);
            System.out.println("Instance = " + instance);
        }
    }

    // load SMO classifier with exception
    private SMO loadClassifier() throws Exception {
        return (SMO) weka.core.SerializationHelper.read(modelFile);
    }

    private void classifyNewInstance(SMO smo, Instances unknownInstances) throws Exception {
        // make copy of new instances
        Instances labeled = new Instances(unknownInstances);

        // label the instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = smo.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        // print labeled
        System.out.println("\nNew, labeled = \n" + labeled);
    }
    // call data file
    void start(String fileName) {
        String datafile = "../../testdata/breast-cancer.arff";
        try {
            // load instance from data file and print them
            Instances instances = loadArff(datafile);
            printInstances(instances);
            // load classifier
            SMO fromFile = loadClassifier();
            // load unknown instances and print them to screen
            Instances unknownInstances = loadArff(fileName);
            System.out.println("\nUnclassified instances = \n" + unknownInstances);
            classifyNewInstance(fromFile, unknownInstances);
        } // use catch with exception
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}

