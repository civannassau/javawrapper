/*
 * Copyright (c) 2019 Chenoa van Nassau
 * All rights reserved
 */

// load packages
package nl.bioinf.civannassau.wekarunner;

// imports
import org.apache.commons.cli.*;

// class commandlineparser
public class CommandlineParser {
    // create everything for commandline use
    private static final String FILE = "file";
    private static final String INSTANCE = "instance";
    private static final String HELP = "help";
    private Options options = new Options();
    private CommandLine commandLine;
    private final String[] clArguments;


    public CommandlineParser(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Initialization and processing.
     */

    private void initialize() {
        createOptions();
        parseCommandLine();
    }

    /**
     * check if help is requested; if so, return true.
     */

    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * commandline arguments.
     */

    private void createOptions() {
        // create Options object
        this.options = new Options();
        Option fileOption = new Option("f", FILE, true, "File was not given.");
        Option instanceOption = new Option("i", INSTANCE, true, "String with instance was not given");
        Option helpOption = new Option("h", HELP, false, "Use -f for file or -i for instance");
        options.addOption(fileOption);
        options.addOption(instanceOption);
        options.addOption(helpOption);
    }

    /**
     * processes the command line arguments.
     */

    private CommandLine parseCommandLine() {
        try {
            org.apache.commons.cli.CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);
            return this.commandLine;
        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    /**
     * get file.
     * return FILE
     */

    public String fetchFile() {
        return this.commandLine.getOptionValue(FILE);
    }

    /**
     * get instance.
     * and return the INSTANCE
     */

    public String getInstance() {
        return this.commandLine.getOptionValue(INSTANCE);
    }

    /**
     * prints help.
     */

    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Help menu", options);
    }
}
