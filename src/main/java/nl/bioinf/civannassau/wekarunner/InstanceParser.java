/*
 * Copyright (c) 2019 Chenoa van Nassau
 * All rights reserved
 */
// load package
package nl.bioinf.civannassau.wekarunner;

// get imports
import java.io.*;

// make class for the instance
public class InstanceParser {
    // change name for use in main file, for better understanding.
    public void Instance(String singInstance) throws IOException {
        //try opening chosen file
        try {
            // find input file
            BufferedReader br = new BufferedReader(new FileReader("../../testdata/input.arff"));
        } catch (FileNotFoundException ex) {
            // if file does not exist, make one.
            PrintWriter writer = new PrintWriter("../../testdata/input.arff", "UTF-8");
        }
            PrintWriter pw = null;
            // Writing the instance file
            try {
                File file = new File("../../testdata/input.arff");
                FileWriter fw = new FileWriter("../../testdata/input.arff", true);
                pw = new PrintWriter(fw);
                pw.print(singInstance);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (pw != null) {
                    pw.close();
                }
            }
        // open file stream to the first file plain.arff
        InputStream is = new FileInputStream("../../testdata/empty.arff");
        byte[] buf = new byte[1 << 20];
        // empty file, so there is no overwriting
        PrintWriter pwp = new PrintWriter("../../testdata/newinstance.arff");
        pwp.print("");
        pwp.close();
        // open file to which file will be joined
        OutputStream os = new FileOutputStream(new File("../../testdata/newinstance.arff"), true);
        int count;
        // read empty.arff file completely and write to newinstance file
        while ((count = is.read(buf)) != -1) {
            os.write(buf, 0, count);
            os.flush();
        }
        is.close();
        // open input.arff
        is = new FileInputStream("../../testdata/input.arff");
        // read entire input.arff and write to newinstance.arff
        while ((count = is.read(buf)) != -1){
            os.write(buf, 0, count);
            os.flush();
        }
        is.close();
        os.close();
        // empty file for the next instance
        PrintWriter pwriter = new PrintWriter("../../testdata/input.arff");
        pwriter.print("");
        pwriter.close();
    }
}
