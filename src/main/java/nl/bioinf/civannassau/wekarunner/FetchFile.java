/*
 * Copyright (c) 2019 Chenoa van Nassau
 * All rights reserved
 */

// load package
package nl.bioinf.civannassau.wekarunner;

// make class to fetch file
public class FetchFile {

    public static void main(String[] args, String fileName){
        Wekarunner wekarunner = new Wekarunner();
        wekarunner.start(fileName);
    }
}
