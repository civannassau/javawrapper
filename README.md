# README #

This readme will be more helpful after cloning the repository, because it contains ways to use the code in the repository to it fullest extend.

This readme will help understand how a java command line application is made that can be run from the command line and how to optimize it to get the best results.

### What is this repository for? ###

* Quick summary
* Version

### How do I get set up? ###

First clone the bitbucket:
* Clone the repository from your bitbucket account to your local computer using the commandline. 
* Use the link: https://bitbucket.org/civannassau/javawrapper/src/master/
* In this repository all the classes are located. They can be run on the commandline using the argument below.

This application classifies new instances. 
To see the made files go to the folder src/main/java/nl/bioinf/civannassau/wekarunner/. 
This foler contains the made classes for the application. 

This can also be seen in the cloned bitbucket. 

The file CommanlineParser.java contains everything that can be asked on the commandline -h, -f, -i. 
The FetchFile.java fetches a file. The InstanceParser.java, when using -i on the commandline it will write the instance to a file.
The Wekarunner.java makes the smo model. The MainClassifierRunner.java is the main, this makes sure that everything can be run on the commandline.
### Contribution guidelines ###

Before testing first go to folder build/libs.
Commandline argument to use in terminal for testing:
1. Go to the directory of the .jar file.
2. Instance in terminal:
    `java -jar untitled-1.0-SNAPSHOT-all.jar -i "insert string with instance"`
    
    File in terminal:
    `java -jar untitled-1.0-SNAPSHOT-all.jar -f 'route_to_file/file' `
    

### Who do I talk to? ###

* Chenoa van Nassau
* c.i.van.nassau@st.hanze.nl  